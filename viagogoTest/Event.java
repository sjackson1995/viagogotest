import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by stephen on 19/04/17.
 * <p>
 * This class stores the data of each event.
 */
public class Event {

    //This stores the co-ordinate on the x-axis of the event
    private int xCoordinate;

    //This stores the co-ordinate on the y-axis of the event
    private int yCoordinate;

    //This stores a list of tickets the event has
    private ArrayList<Double> tickets;

    //The id of the event
    private int eventId;

    /**
     * Creates the Event object with all the specified parameters
     *
     * @param xCoordinate The x co-ordinate of the position of the event
     * @param yCoordinate The y co-ordinate of the position of the event
     * @param tickets The list of tickets stored for this event
     * @param eventId The unique event ID
     */
    public Event(int xCoordinate, int yCoordinate, ArrayList<Double> tickets, int eventId) {
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
        this.tickets = tickets;
        this.eventId = eventId;
    }

    public int getxCoordinate() {
        return xCoordinate;
    }

    public int getyCoordinate() {
        return yCoordinate;
    }

    public int getEventId() {
        return eventId;
    }


    /**
     * This will sort the list of tickets and then return it with a dollar representation
     *
     * @return A String which is the cheapest ticket
     */
    public String getCheapestTicket() {

        if (tickets.size() == 0) return null;
        Collections.sort(tickets);
        String cheapestTicket = String.format("$%,.2f", tickets.get(0));
        return cheapestTicket;

    }


    /**
     * Override this method so HashSet can check it and ensure no two x and y co-ordinates are the same
     *
     * @param o An event object used in HashSet used to check if it equals in object or x/y co-ordinates
     * @return Whether this object is the same as object passed in
     */
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        Event event = (Event) o;
        //Return true if both co-ordinates are the same, so not added to HashSet
        return (yCoordinate == event.yCoordinate && xCoordinate == event.xCoordinate);
    }

    /**
     * Override the hashCode to create a unique one for x and y co-ordinate to ensure no two are the same
     *
     * @return The unique int hashcode created based on x and y co-ordinate
     */
    @Override
    public int hashCode() {
        int result = xCoordinate;
        result = 31 * result + yCoordinate;
        return result;
    }


}
