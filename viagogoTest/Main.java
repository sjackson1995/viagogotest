import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

/**
 * Created by stephen on 19/04/17.
 * This class is where the main method is called to start the program
 * In addition, in the Main class is where we sort the Events out by manhattan and print the closest 5
 */
public class Main {

    //The random seed data created.
    private ArrayList<Event> eventSeedData;

    public Main(ArrayList<Event> eventSeedData) {

        this.eventSeedData = eventSeedData;

    }

    /**
     * Change the ticket part, so it's an ArrayList or so of tickets. It sounds like there is more than 1
     * different ticket price. Maybe a ticket object with price
     * Also add check for console input to not go outside range of -10 < and > 10
     *
     * @param args Not used in this program
     */
    public static void main(String[] args) {

        System.out.println("Please input co-ordinates with the format of: 'x,y' with no quotes");
        System.out.println("Type 'exit' with no quotes to exit the program");

        //Creates the seed data on program load and passes it to ArrayList
        GenerateSeedData generateSeedData = new GenerateSeedData();
        ArrayList<Event> eventSeedData = generateSeedData.createSeedData();

        String input;

        Main main = new Main(eventSeedData);

        //Will continue to loop around, getting input from the user, doing some checks to check input is valid
        //then sort and print top 5 closest events
        while (true) {
            Scanner scan = new Scanner(System.in);
            input = scan.next();

            if (input.toLowerCase().trim().equals("exit"))
                break;

            //X and Y passed in from the console
            int x;
            int y;

            try {
                //Converts x and y to integers, breaking it up on the ,
                //If the comma is not present or the x and y are not numbers, will be caught by the
                //catch and then continue to next loop
                x = Integer.valueOf(input.split(",")[0]);
                y = Integer.valueOf(input.split(",")[1]);

            } catch (java.lang.NumberFormatException | java.lang.ArrayIndexOutOfBoundsException e) {
                System.out.println("Please only enter numbers in this format: 'x,y' with no quotes ");
                continue;
            }

            //Check we are within the bounds
            if ((x < -10 || x > 10) || (y < -10 || y > 10)) {
                System.out.println("x and y must be in-between -10 and 10");
                continue;
            }

            main.manhattanDistance(x, y);


        }

    }

    /**
     * Being passed two co-ordinates, it sorts the ArrayList by manhattan distance
     * and then prints out the first 5 closest to the x and y co-ordinate
     * @param x The x co-ordinate the user typed in
     * @param y The y co-ordinate the user typed in
     */
    public void manhattanDistance(int x, int y) {

        /**
         * Using the x and y co-ordinate passed in from console, do a custom
         * sort based on Manhattan distance
         */
        Collections.sort(eventSeedData, new Comparator<Object>() {

            @Override
            public int compare(Object o1, Object o2) {
                Event e1 = (Event) o1;
                Event e2 = (Event) o2;
                //Manhattan distance - compare two elements in ArrayList to sort
                int dist1 = Math.abs(x - e1.getxCoordinate()) + Math.abs(y - e1.getyCoordinate());
                int dist2 = Math.abs(x - e2.getxCoordinate()) + Math.abs(y - e2.getyCoordinate());
                return Integer.compare(dist1, dist2);

            }
        });


        /**
         * Print out the top 5 places (this will be the first 5 of the ArrayList) as the closest events
         * or the ArrayList size if it's less than 5
         */
        int loopSize = (eventSeedData.size() > 4) ? 5 : eventSeedData.size();

        System.out.println("Closest events to (" + x + "," + y + "):");
        for (int i = 0; i < loopSize; i++) {

	        Event currentEvent = eventSeedData.get(i);
            int dist = Math.abs(x - currentEvent.getxCoordinate()) + Math.abs(y - currentEvent.getyCoordinate());

            //In case of no tickets for event, don't print out null
            String ticket = currentEvent.getCheapestTicket() == null ? "No ticket for this event" : currentEvent.getCheapestTicket();

            System.out.println("Event: " + currentEvent.getEventId() + " - " + ticket + ", "
                    + "Distance " + dist);
        }

    }


}
