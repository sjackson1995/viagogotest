import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

/**
 * Created by stephen on 19/04/17.
 * This class creates the random seed data that is used by the Main class
 */
public class GenerateSeedData {


    /**
     * This returns an ArrayList of objects Event which is created using random
     * data (using milliseconds as a seed to ensure it is more random). The length
     * of the ArrayList and data inside will be different on each call
     *
     * @return ArrayList of the type Event
     */
    public ArrayList<Event> createSeedData() {

        //Using a HashSet to ensure we don't get duplicates (we have overrided method
        //in Event class to check for duplicate x and y)
        HashSet<Event> eventSeedData = new HashSet<>();

        //Randomly decides how many events to create (can be less if duplicate created before going into HashSet)
        //We always get at least 20 for some form of data
        Random seedData = new Random(System.currentTimeMillis());
        int howManyEventsToCreate = seedData.nextInt(30) + 21;

        for (int event = 1; event < howManyEventsToCreate; event++) {

            //This ensures we always get between -10 and 10
            int xCoordinate = seedData.nextInt(21) - 10;
            int yCoordinate = seedData.nextInt(21) - 10;

            Event newEvent = new Event(xCoordinate, yCoordinate, generateTickets(seedData), event);
            eventSeedData.add(newEvent);

        }

        //Convert back to ArrayList as easier to sort later
        return new ArrayList<>(eventSeedData);
    }

    /**
     * Returns an ArrayList of randomly generated ticket prices to be added to the Event object
     *
     * @param randomSeedData
     * @return An ArrayList of tickets to be added to Event object
     */
    private ArrayList<Double> generateTickets(Random randomSeedData) {

        ArrayList<Double> tickets = new ArrayList<>();

        int howManyTickets = randomSeedData.nextInt(50);

        for (int ticket = 0; ticket < howManyTickets; ticket++) {

            //Price will be at least 30 dollars. Just a random number picked
            tickets.add((randomSeedData.nextDouble() * 100) + 30);
        }

        return tickets;
    }
}
